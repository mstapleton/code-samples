<?php

class Event_NoteController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $this->view->title = "Event Note";
        $this->view->headTitle($this->view->title, 'PREPEND');

        $eventNote = new Event_Model_DbTable_Event_Note();
        $this->view->eventNote = $eventNote->fetchAll();
    }

    // add event note
    public function addAction()
    {
        $this->_helper->layout->disableLayout();
        
        $this->view->title = "Add event note";
        $this->view->headTitle($this->view->title, 'PREPEND');
        $eventNote = new Event_Model_DbTable_Event_Note();
        $events = new Event_Model_DbTable_Events();
		
		$event_id = $this->_getParam('id', 0);
        $eventItem = $events->getEvent($event_id);

		$csNamespace = new Zend_Session_Namespace('ClientSelection');
		if ($id)
		{
            $csNamespace->client_id = $eventItem['client_id'];
	        $csNamespace->event_id = $event_id;
		}
        $form = new Event_Form_Event_Note();

        $form->id->setValue($event_id);

        
        $form->submit->setLabel('Add');
        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
        }
    }
    
    public function viewAction()
    {
        $this->_helper->layout->disableLayout();
        
        $event_id = $this->_getParam('id', 0);
        $noteModel = new Event_Model_DbTable_Event_Note();
        $notes = $noteModel->getEventNotesById($event_id);
        $this->view->notes = $notes;
    }

    public function editAction()
    {
        $this->view->title = "Edit event note";
        $this->view->headTitle($this->view->title, 'PREPEND');
        $eventNote = new Event_Model_DbTable_Event_Note();
        $events = new Event_Model_DbTable_Events();
		
		$id = $this->_getParam('id', 0);
        $eventItem = $events->getEvent($id);
		$eventNoteItem = $eventNote->getEventNote($id);

		$csNamespace = new Zend_Session_Namespace('ClientSelection');
		if ($id)
        {
            $csNamespace->client_id = $eventItem['client_id'];
        }
        
        $form = new Event_Form_Event_Note();
        $form->submit->setLabel('Save');
        $this->view->form = $form;
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            
            if ($form->isValid($formData)) {
                $id = $form->getValue('id');
                $note = $form->getValue('note');
                $modified = $form->getValue('modified');
                $eventNote->updateEventNote($id, $note, $modified);
            } 
            else {
                $form->populate($formData);
            }
        }
        else
        {
            $id = $this->_getParam('id', 0);
            if ($id > 0)
            {
                $eventNote = new Event_Model_DbTable_Event_Note();
                $form->populate($eventNote->getEventNote($id));
            }
        }
    }

    public function deleteAction()
    {
        $this->view->title = "Delete event note";
        $this->view->headTitle($this->view->title, 'PREPEND');
        
        if ($this->getRequest()->isPost()) {
            
            $del = $this->getRequest()->getPost('del');
            
            if ($del == 'Yes') {
                $id = $this->getRequest()->getPost('id');
                $eventNote = new Event_Model_DbTable_Event_Note();
                $eventNote->deleteEventNote($id);
            }
        } 
        else
        {
            $id = $this->_getParam('id', 0);
            $eventNote = new Event_Model_DbTable_Event_Note();
            $this->view->eventNote = $eventNote->getEventNote($id);
        }
    }

}
