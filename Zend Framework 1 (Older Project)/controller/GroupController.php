<?php

class Event_GroupController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    // load list of all event groups
    public function indexAction()
    {
        $this->view->title = "Event Groups";
        $this->view->headTitle($this->view->title, 'PREPEND');

        $eventGroups = new Event_Model_DbTable_Event_Groups();
        $this->view->eventGroups = $eventGroups->fetchAll();
    }

    // add new event group
    public function addAction()
    {
        $this->view->title = "Add new event group";
        $this->view->headTitle($this->view->title, 'PREPEND');
        $form = new Event_Form_Event_Group();
        $form->submit->setLabel('Add');
        $this->view->form = $form;
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
        
            if ($form->isValid($formData)) {
                $name = $form->getValue('name');
                $eventGroups = new Event_Model_DbTable_Event_Groups();
                $eventGroups->addEventGroup($name);
                $this->_redirect('/event/group');
            } 
            else {
                $form->populate($formData);
            }
        }
    }

    // edit existing event group
    public function editAction()
    {
        $this->view->title = "Edit event group";
        $this->view->headTitle($this->view->title, 'PREPEND');
        $form = new Event_Form_Event_Group();
        $form->submit->setLabel('Save');
        $this->view->form = $form;
        
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
        
            if ($form->isValid($formData)) {
                $id = $form->getValue('id');
                $name = $form->getValue('name');
                $modified = $form->getValue('modified');
                $eventGroups = new Event_Model_DbTable_Event_Groups();
                $eventGroups->updateEventGroup($id, $name, $modified);
                $this->_redirect('/event/group');
            } 
            else {
                $form->populate($formData);
            }
        }
        else
        {
            $id = $this->_getParam('id', 0);
            if ($id > 0)
            {
                $eventGroups = new Event_Model_DbTable_Event_Groups();
                $form->populate($eventGroups->getEventGroup($id));
            }
        }
    }

    // delete event group
    public function deleteAction()
    {
        $this->view->title = "Delete event group";
        $this->view->headTitle($this->view->title, 'PREPEND');
        
        if ($this->getRequest()->isPost()) {
            
            $del = $this->getRequest()->getPost('del');
            
            if ($del == 'Yes') {
                $id = $this->getRequest()->getPost('id');
                $eventGroups = new Event_Model_DbTable_Event_Groups();
                $eventGroups->deleteEventGroup($id);
            }
            $this->_redirect('/event/group');
        } 
        else
        {
            $id = $this->_getParam('id', 0);
            $eventGroups = new Event_Model_DbTable_Event_Groups();
            $this->view->eventGroup = $eventGroups->getEventGroup($id);
        }
    }


}
