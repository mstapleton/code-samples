<?php

class Event_Model_DbTable_Event_Groups extends Zend_Db_Table_Abstract
{
    protected $_name = 'event_groups';
    
    public function getEventGroup($id)
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) {
            throw new Exception("Could not find group id $id");
        }
        return $row->toArray();
    }
    
    public function getEventGroups()
    {
    	$rowSet = $this->fetchAll("active = true");
        if (!$rowSet) {
            throw new Exception("Could not find template field types");
        }

        return $rowSet;
    }
    
    public function addEventGroup($name)
    {
        $data = array(
            'name' => $name
        );
        $this->insert($data);
    }
    
    public function updateEventGroup($id, $name, $modified)
    {
        $data = array(
            'name' => $name,
            'modified' => 'now()',
        );
        $this->update($data, 'id = '. (int)$id);
    }

    public function deleteEventGroup($id)
    {
        $this->delete('id =' . (int)$id);
    }
}
