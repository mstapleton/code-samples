<?php

class Event_Model_DbTable_Event_Note extends Zend_Db_Table_Abstract
{
    protected $_name = 'event_note';
    
    public function getEventNote($id)
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if ($row)
            return $row->toArray();
	    else
		    return false;
    }
    
    public function getEventNotes()
    {
    	$rowSet = $this->fetchAll("active = true");
        return $rowSet;
    }
    
    public function getEventNotesById($event_id)
    {
        $select = $this->getAdapter()->select()
                      ->from(array('en'=>'event_note'),
                             array('*'))
                      ->join(array('u'=>'users'),
                             'u.id = en.created_by_id')
                      ->where('en.event_id = '.(int)$event_id);
        $rowSet = $this->getAdapter()->fetchAll($select);
        return $rowSet;
    }
    
    public function getEventNoteCount($event_id)
    {
        $select = $this->select();
        $select->from($this->_name,'COUNT(*) AS num');
        $select->where("event_id = ".$event_id);
        return $this->fetchRow($select)->num;
    }
    
    public function addEventNote($event_id, $note, $created_by_id=6, $modified_by_id=6)
    {
        $data = array(
		    'id' => $id,
            'event_id' => $event_id,
            'note' => $note,
            'created_by_id' => $created_by_id,
            'modified_by_id' => $modified_by_id,
            'created_on' =>  new Zend_Db_Expr('NOW()'),
            'modified_on' =>  new Zend_Db_Expr('NOW()')
        );
        $this->insert($data);
    }
    
    public function updateEventNote($id, $note)
    {
        $data = array(
            'id' => $id,
            'note' => $note,
            'modified_on' =>  new Zend_Db_Expr('NOW()')
        );
        $this->update($data, 'id = '. (int)$id);
    }

    public function deleteEventNote($id)
    {
        $this->delete('id =' . (int)$id);
    }
}
