<?php

class Event_Form_Event_Note extends Zend_Form
{
    
    public function __construct($options = null)
    {
        parent::__construct($options);
        $this->setName('eventNote');
        
        $id = new Zend_Form_Element_Hidden('id');
        $event_id = new Zend_Form_Element_Hidden('event_id');
        
        $note = new Zend_Form_Element_Textarea('note');
        $note->setLabel('Note')
                ->setAttrib('cols',35)
                ->setAttrib('rows',15);
                
        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setAttrib('id', 'submitbutton');
        $this->addElements(array($id, $event_id, $note, $submit));
    }
}
