<?php
class Event_Form_Event_Template extends Zend_Form
{
    
    public function __construct($options = null)
    {
        parent::__construct($options);
        $this->setName('eventTemplate');
        
        $id = new Zend_Form_Element_Hidden('id');
        
        $name = new Zend_Form_Element_Text('name');
        $name->setLabel('Name')
                  ->setRequired(true)
                  ->addFilter('StripTags')
                  ->addFilter('StringTrim')
                  ->addValidator('NotEmpty');
        
        $listOptions = array(
        	'1' => 'Client',
        	'2' => 'Care Giver',
        	'3' => 'Professional Staff',
        	'4' => 'Administrator'
        );
        
        $user_role = new Zend_Form_Element_Select('user_role');
        $user_role->setLabel('User Class')
                        ->setMultiOptions($listOptions)
                        ->setRequired('true')
                        ->addFilter('StringTrim')
                        ->addValidator('NotEmpty');
              
        $listOptions = array(
        	'0' => 'One Time',
        	'1' => 'Days',
        	'2' => 'Yearly'
        );
        
        $frequency = new Zend_Form_Element_Select('frequency');
        $frequency->setLabel('Frequency')
                        ->setMultiOptions($listOptions)
                        ->setRequired('true')
                        ->addFilter('StringTrim')
                        ->addValidator('NotEmpty');
              
        $period = new Zend_Form_Element_Text('period');
        $period->setLabel('Period')
                  ->setRequired(true)
                  ->addFilter('StripTags')
                  ->addFilter('StringTrim')
                  ->addValidator('NotEmpty');
        
        $active = new Zend_Form_Element_Checkbox('active');
        $active->setLabel('Active?')
                  ->setRequired('true')
                  ->addFilter('StringTrim')
                  ->addValidator('NotEmpty');
        
        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setAttrib('id', 'submitbutton');
        $this->addElements(array($id, $name, $user_role, $frequency, $period, $active, $submit));
    }
}
